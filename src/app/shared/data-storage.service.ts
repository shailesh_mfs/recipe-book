import { Injectable } from "@angular/core";
import { RecipeService } from "../recipes/recipe.service";
import 'rxjs/Rx';
import { Recipe } from "../recipes/recipe.modal";
import { AuthService } from "../auth/auth.service";
import { HttpClient, HttpHeaders, HttpParams, HttpRequest } from "@angular/common/http";


@Injectable()
export class DataStorageService{

    constructor(private httpClient:HttpClient, private recipeService: RecipeService, private authService:AuthService){

    }

    storeRecipes(){
        // return this.httpClient.put("https://ng-recipe-book-3baa8.firebaseio.com/recipes.json", this.recipeService.getRecipes(),
        // {headers:headers,
        // observe:'body',
        // params: new HttpParams().set('auth',token)
        const req = new HttpRequest('PUT','https://ng-recipe-book-3baa8.firebaseio.com/recipes.json',
        this.recipeService.getRecipes(),
        {'reportProgress':true});
       return this.httpClient.request(req);   
    }

    getRecipes(){
        const token = this.authService.getToken();
        const header = new HttpHeaders().set('Content-Type','application/json');       
        return this.httpClient.get<Recipe[]>("https://ng-recipe-book-3baa8.firebaseio.com/recipes.json",{observe:'body',
        responseType:'json',
        headers:header,
        params: new HttpParams().set('auth',token)})
        // const req = new HttpRequest('GET','https://ng-recipe-book-3baa8.firebaseio.com/recipes.json');
        // return this.httpClient.request(req)
        .map(
            (recipes)=>{
                for(let recipe of recipes ){
                    if(!recipe['ingredients']){
                        recipe['ingredients']=[];
                    }
                }
                return recipes;
            }
        )
        .subscribe(
            (recipes:Recipe[])=>{                
                this.recipeService.setRecipes(recipes);
            }
        )
        
    }
}