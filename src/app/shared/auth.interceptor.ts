import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from "@angular/common/http";
import { Observable } from "rxjs/Observable";
import { Injectable } from "@angular/core";
import { AuthService } from "../auth/auth.service";

@Injectable()
export class AuthInterceptor implements HttpInterceptor{
    constructor(private authService:AuthService){}

    intercept(req:HttpRequest<any>,next:HttpHandler): Observable<HttpEvent<any>>{
        console.log('Intercepted! '+req);
        const token = this.authService.getToken();
        const cloneReq = req.clone({'headers': req.headers.set('Content-Type','application/json'),
        'params': req.params.set('auth',token)});
        return next.handle(cloneReq);

    }

}