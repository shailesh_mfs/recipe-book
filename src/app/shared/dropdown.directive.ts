import { Directive, ElementRef, Renderer2, OnInit, HostBinding, HostListener } from '@angular/core';

@Directive({
    selector:'[appDropdown]'
})
export class DropdownDirective implements OnInit{
    // Method 1
    // constructor(private eleRef:ElementRef,private rendere:Renderer2){}

    // @HostListener('mouseenter') mouseover(eventData:Event){
    //     this.rendere.addClass(this.eleRef.nativeElement,'open');
    // }

    // @HostListener('mouseleave') mouseleave(eventData:Event){
    //     this.rendere.removeClass(this.eleRef.nativeElement,'open');
    // }

    // Method 2
    @HostBinding('class.open') isOpen = false;
    
    @HostListener('click') toggleOpen(){
        this.isOpen = !this.isOpen;
    }

    ngOnInit(){}
}