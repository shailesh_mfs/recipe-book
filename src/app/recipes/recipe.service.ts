import { Recipe } from './recipe.modal';
import { Injectable } from '@angular/core';
import { Ingrdient } from '../shared/ingredient.modal';
import { ShoppingListService } from '../shopping-list/shopping-list.service';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class RecipeService{
    constructor(private shoppingListService:ShoppingListService){}
    recipesChanged = new Subject<Recipe[]>();
    
       private recipes: Recipe[]=[
            new Recipe('Noodle',
            'This is a test recipe',
            'https://image.shutterstock.com/image-photo/schezwan-veg-noodles-spicy-tasty-450w-1102930289.jpg',
            [
                new Ingrdient('Vegetables',30),
                new Ingrdient('Onion',10)
            ]),
            new Recipe('Hakka Noodle',
            'This is a test recipe1',
            'https://image.shutterstock.com/image-photo/hand-uses-chopsticks-pickup-tasty-450w-721269526.jpg',
            [
                new Ingrdient('Vegetables',30),
                new Ingrdient('Onion',10),
                new Ingrdient('Honey',15)
            ])
          ];

      getRecipes(){
          return this.recipes.slice();
      }

      setRecipes(recipe:Recipe[]){
          this.recipes = recipe;
          this.recipesChanged.next(this.recipes.slice());
      }

      getRecipe(index:number){
          return this.recipes[index];
      }

      addIngredientToShoppingList(ingredient:Ingrdient[]){
        this.shoppingListService.addIngredients(ingredient);
      }

      addRecipe(recipe:Recipe){
          this.recipes.push(recipe);
          this.recipesChanged.next(this.recipes.slice());
      }

      updateRecipe(index, newRecipe: Recipe){
          this.recipes[index] = newRecipe;
          this.recipesChanged.next(this.recipes.slice());
      }

      deleteRecipe(index:number){
          this.recipes.splice(index,1);
          this.recipesChanged.next(this.recipes.slice());
      }
}