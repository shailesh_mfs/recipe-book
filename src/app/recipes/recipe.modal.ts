import { Ingrdient } from "../shared/ingredient.modal";

export class Recipe{
    public name:string;
    public description:string;
    public imagePath:string;
    public ingredients:Ingrdient[];

    constructor(name:string, desc:string,imagePath:string, ingredient:Ingrdient[]){
        this.name = name;
        this.description = desc;
        this.imagePath = imagePath;
        this.ingredients = ingredient;
    }
}