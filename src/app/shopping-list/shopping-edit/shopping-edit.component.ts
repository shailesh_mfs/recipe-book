import { Component, OnInit, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { Ingrdient } from '../../shared/ingredient.modal';
import { ShoppingListService } from '../shopping-list.service';
import { NgForm } from '@angular/forms';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {
  subscription:Subscription;
  editMode=false;
  editModeIndex:number;
  editedItem:Ingrdient;

    @ViewChild('f') shoppingFormData:NgForm;
  
  constructor(private shoppingListService:ShoppingListService) { }

  onSubmit(form:NgForm){
      const value = form.value;
     const newIngedient = new Ingrdient(value.name,value.amount);
     if(this.editMode){
       this.shoppingListService.updateIngredients(this.editModeIndex,newIngedient);
     } else{
      this.shoppingListService.addIngredient(newIngedient);
     }
     form.reset();
     this.editMode = false;
    
  }

  onClear(){
    this.shoppingFormData.reset();
    this.editMode = false;
  }

  onDelete(){    
    this.shoppingListService.deleteIngredients(this.editModeIndex);
    this.onClear();
  
  }

  ngOnInit() {
    this.subscription = this.shoppingListService.startedEditing.subscribe(
      (index:number)=>{
          this.editMode = true;
          this.editModeIndex = index;
          this.editedItem = this.shoppingListService.getIngredient(index);
          this.shoppingFormData.setValue({
            name:this.editedItem.name,
            amount:this.editedItem.amount
          })
      }
    );
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

}
