import { Component, OnInit, OnDestroy } from '@angular/core';
import { Ingrdient } from '../shared/ingredient.modal';
import { ShoppingListService } from './shopping-list.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-shopping-list',
  templateUrl: './shopping-list.component.html',
  styleUrls: ['./shopping-list.component.css'],
  })
export class ShoppingListComponent implements OnInit, OnDestroy {
  ingredients:Ingrdient[];
  private subscription:Subscription;

  constructor(private shoppingListService:ShoppingListService) { }
  
  ngOnInit() {
    this.ingredients = this.shoppingListService.getIngredients();
    this.subscription = this.shoppingListService.ingredientChange.subscribe(
      (ingredient:Ingrdient[])=>{
        this.ingredients = ingredient;
      }
    );
  }

  onEditItem(index:number){
  this.shoppingListService.startedEditing.next(index);
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

}
