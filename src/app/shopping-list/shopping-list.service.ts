import { Ingrdient } from '../shared/ingredient.modal';
import { Subject } from 'rxjs/Subject';

export class ShoppingListService{
    ingredientChange = new Subject<Ingrdient[]>();
    startedEditing = new Subject<number>();
    private ingredients:Ingrdient[] = [
        new Ingrdient('Apple',5),
        new Ingrdient('Tomato',10),
      ];

      getIngredients(){
          return this.ingredients.slice();
      }

      getIngredient(index:number){
        return this.ingredients[index];
    }

      addIngredient(newIngredient:Ingrdient){
          this.ingredients.push(newIngredient);
          this.ingredientChange.next(this.ingredients.slice());
      }

      addIngredients(ingredients:Ingrdient[]){
        //   Approach 1
        //   for(let ingredient of ingredients){
        //       this.addIngredient(ingredient);
        //   }

        // Approach 2 (recommended)
          this.ingredients.push(...ingredients);
          this.ingredientChange.next(this.ingredients.slice());
      }

      updateIngredients(index:number,newIngredient:Ingrdient){
            this.ingredients[index]= newIngredient;
            this.ingredientChange.next(this.ingredients.slice());
      }

      deleteIngredients(index:number){
          this.ingredients.splice(index,1);
          this.ingredientChange.next(this.ingredients.slice());
      }
}