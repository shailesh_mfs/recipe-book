import { Component, OnInit } from '@angular/core';
import { DataStorageService } from '../../shared/data-storage.service';
import { RecipeService } from '../../recipes/recipe.service';
import { AuthService } from '../../auth/auth.service';
import { Router } from '@angular/router';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {


  constructor(private firebaseService:DataStorageService, private recipeService:RecipeService,
     private authService:AuthService, private router:Router) { }

  
  ngOnInit() {
  }

  onSaveRecipes(){
    this.firebaseService.storeRecipes().subscribe(
      (response)=>console.log(response),
      (error)=>console.log(error)
    )
  }

  isAuthenticated() {
    return this.authService.isAuthenticated();
}

  onGetRecipes(){
    this.firebaseService.getRecipes();
  }

  onLogout(){
    this.authService.logout();
    this.router.navigate(['signin']);
  }

}
