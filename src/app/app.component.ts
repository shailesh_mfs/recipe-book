import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'udemy-course-project';
  renderRoute:string = 'Recipes';
  

  ngOnInit(){
    firebase.initializeApp({apiKey: "AIzaSyABkCglZx6-74X2usdZT7X9FznggUti1WU",
    authDomain: "ng-recipe-book-3baa8.firebaseapp.com"})
  }
  renderComponent(routeName:string){
    this.renderRoute=routeName;
    }
}
